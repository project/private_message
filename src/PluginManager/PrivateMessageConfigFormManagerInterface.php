<?php

declare(strict_types=1);

namespace Drupal\private_message\PluginManager;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Plugin Manager to detect PrivateMessageConfigForm plugins.
 */
interface PrivateMessageConfigFormManagerInterface extends PluginManagerInterface {}
